## Examples

```
scrapy crawl summary -a gender=F -a profile_type=10723,10724 -a age_from=20 -a age_to=30 -a country=US -a sort=logintime -a last=10
scrapy crawl full -a gender=F -a profile_type=10723,10724 -a age_from=20 -a age_to=30 -a country=US -a sort=logintime -a last=10
```

gender:

```
F (default)
M
```

profile_type:

```
10723 = sperm donor (default)
10724 = woman want sperm donor (default)
```

Default age is from 19 to 40.

country:

```
AA = All Countries (default)
US = United States
```

sort:

```
regdate (default)
logintime
```
