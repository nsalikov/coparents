from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote

def _strip(string, default=None):
    if string:
        return string.strip()
    return default


def add_domain(domain, url):
    if not url.startswith('http'):
        return domain + url
    else:
        return url


def fix_image_url(url):
    p = list(urlparse(url))
    q = dict(parse_qsl(p[4]))

    if 'picid' not in q or q['picid'] is None:
        return None

    q2 = {'id': q['id'], 'picid': q['picid']}

    p[4] = urlencode(q2)
    url = urlunparse(p)

    return url

def is_login(response):
    url = response.url

    check_login_css = 'div.acouds'
    if not response.css(check_login_css).extract_first():
        return False

    if 'login.php' in url:
        return False

    if 'page=login' in url:
        return False

    if 'signup.php' in url:
        return False

    return True


def handle_params(self):
    if hasattr(self, 'gender') and self.gender:
        self.gender = self.gender.strip().upper()
    else:
        self.gender = 'F'

    if hasattr(self, 'profile_type') and self.profile_type:
        self.profile_type = [t.strip() for t in self.profile_type.split(',')]
    else:
        self.profile_type = [10723, 10724]

    if hasattr(self, 'age_from') and self.age_from:
        self.age_from = self.age_from.strip()
    else:
        self.age_from = 19

    if hasattr(self, 'age_to') and self.age_to:
        self.age_to = self.age_to.strip()
    else:
        self.age_to = 40

    if hasattr(self, 'country') and self.country:
        self.country = self.country.strip().upper()
    else:
        self.country = 'AA'

    if hasattr(self, 'sort') and self.sort:
        # logintime | regdate
        self.sort = self.sort.strip().lower()
    else:
        self.sort = 'regdate'

    if hasattr(self, 'last') and self.last:
        self.last = int(self.last)
    else:
        self.last = 1000000

    pt = '&'.join(['question%5B38%5D%5B%5D={}'.format(t) for t in self.profile_type])

    self.search_url = self.search_url.format(gender=self.gender, profile_type=pt, age_from=self.age_from, age_to=self.age_to, country=self.country)


def fix_sortorder(url, sortorder):

    p = list(urlparse(url))
    q = dict(parse_qsl(p[4]))

    q['sort_by'] = sortorder

    p[4] = urlencode(q)
    url = urlunparse(p)

    return url


def get_recipient_id(response):
    url = response.css('div.main_img img ::attr(src)').extract_first()

    if not url:
        return None

    if not url.startswith('http'):
        url = 'https://coparents.com/' + url.lstrip('/')

    p = list(urlparse(url))
    q = dict(parse_qsl(p[4]))

    if 'id' in q:
        return q['id']
    else:
        return None
