# -*- coding: utf-8 -*-
import re
import scrapy
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from ..helpers import _strip, add_domain, fix_image_url, is_login, handle_params, fix_sortorder


class SearchSpider(scrapy.Spider):
    name = 'full-autologin'
    allowed_domains = ['coparents.com']
    search_url = 'https://www.coparents.com/advsearch.php?cursectionid=0&sectionid=0&userid=&advsearch=Search&chgcntry=&srchusername=&srchlookgender%5B%5D={gender}&{profile_type}&srchlookage={age_from}%3B{age_to}&srchlookcountry={country}&srchlookcounty=AA&srchlookstate_province=&srchlookcity=&srchlookzip=&results_per_page=50'

    custom_settings = {
        'DOWNLOADER_MIDDLEWARES': {
            'autologin_middleware.AutologinMiddleware': 605,
            'scrapy.downloadermiddlewares.cookies.CookiesMiddleware': None,
            'autologin_middleware.ExposeCookiesMiddleware': 700,
        }
    }


    def start_requests(self):
        self.date = datetime.now().isoformat()

        handle_params(self)

        yield scrapy.Request(self.search_url, callback=self.set_sortorder, dont_filter=True)


    def set_sortorder(self, response):
        # inspect_response(response, self)

        pages_css = 'div.pating a ::attr(href)'
        first_page = response.css(pages_css).extract_first()

        if not first_page:
            raise CloseSpider("Search is empty")

        first_page = fix_sortorder(first_page, self.sort)

        return response.follow(first_page, callback=self.parse_search, dont_filter=True)


    def parse_search(self, response):
        # inspect_response(response, self)

        if not is_login(response):
            yield response.follow(response.request.url, callback=self.parse_search, dont_filter=True)
            return

        items_css = 'div.my_srch div.frsoi'
        items = response.css(items_css)

        for item in items:
            if self.last > 0:
                self.last = self.last - 1

                url = item.css('div.ovidois h4 a ::attr(href)').extract_first()
                last_login = item.css('div.uper_woid font ::text').extract_first()
                last_login = last_login.replace('Last login: ', '').strip() if last_login else ''
                meta = {'last_login': last_login}

                yield response.follow(url, meta=meta, callback=self.parse_profile)

        pages_css = 'div.pating a ::attr(href)'
        pages = response.css(pages_css).extract()

        for page in pages:
            if self.last > 0:
                yield response.follow(page, callback=self.parse_search)


    def parse_profile(self, response):
        # inspect_response(response, self)

        if not is_login(response):
            yield response.follow(response.request.url, callback=self.parse_profile, dont_filter=True)
            return

        d = {}

        d['scraping_date'] = self.date
        d['url'] = response.url

        d['name'] = _strip(' '.join(response.css('div.tiblke h1 ::text').extract()))
        d['last_login'] = response.meta['last_login']

        (sex, age, location) = [re.sub('[\r\n\t\xa0 ]+', ' ', text).strip() for text in response.css('div.tiblke span ::text').extract()]
        d['sex'] = sex
        d['age'] = age
        d['location'] = location
        d['looking'] = '\n'.join(filter(None, [re.sub('[\r\n\t\xa0 ]+', ' ', text).strip() for text in response.css('div.tiblke div.imglpog ::text').extract()]))
        d['description'] = '\n'.join(filter(None, [re.sub('[\r\n\t\xa0 ]+', ' ', text).strip() for text in response.css('div.heding p.pto ::text').extract()]))

        d['details'] = {}

        lis = response.css('div.heding ul li')
        for li in lis:
            label = _strip(' '.join(filter(None, [s.strip().rstrip(':') for s in li.css('label ::text').extract()])))
            value = _strip('\n'.join(filter(None, [s.strip() for s in li.css('b ::text').extract()])))

            if label and value:
                d['details'][label] = value

        d['image_urls'] = response.css('div.main_img img ::attr(src)').extract()
        d['image_urls'].extend(response.css('div.sld_img img ::attr(longdesc)').extract())

        d['image_urls'] = [add_domain('https://www.coparents.com/', img) for img in d['image_urls']]
        d['image_urls'] = list(filter(None, [fix_image_url(img) for img in d['image_urls']]))

        yield d
