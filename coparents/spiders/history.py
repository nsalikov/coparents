# -*- coding: utf-8 -*-
import re
import scrapy
import dateparser
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode, quote
from ..helpers import _strip, is_login


class HistorySpider(scrapy.Spider):
    name = 'history'
    allowed_domains = ['coparents.com']
    login_url = 'https://www.coparents.com/login.php'
    urls = [
        ('views/received', 'https://www.coparents.com/listviewswinks.php?type=&sort=&act=V&folder=received&page=1'),
        # ('views/sent', 'https://www.coparents.com/listviewswinks.php?sort=&type=&act=V&folder=sent&page=1'),
        ('winks/received', 'https://www.coparents.com/listviewswinks.php?sort=&type=&act=W&folder=received&page=1'),
        ('winks/sent', 'https://www.coparents.com/listviewswinks.php?sort=&type=&act=W&folder=sent&page=1'),
        ('favorites/received', 'https://www.coparents.com/buddybanlist.php?sort=&type=&act=F&folder=received&show=1&page=1'),
        ('favorites/sent', 'https://www.coparents.com/buddybanlist.php?sort=&type=&act=F&folder=sent&show=1&page=1'),
    ]

    custom_settings = {
        'DOWNLOAD_DELAY': 1,
        'ITEM_PIPELINES': {},
    }


    def start_requests(self):
        self.login = self.settings.get('AUTOLOGIN_USERNAME')
        self.password = self.settings.get('AUTOLOGIN_PASSWORD')

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'txtusername': self.login,
                    'txtpassword': self.password,
                    'rememberme': 'on',
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if is_login(response):
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        for _type, url in self.urls:
            yield scrapy.Request(url, meta={'_type': _type}, callback=self.parse)


    def parse(self, response):
        # inspect_response(response, self)

        _type = response.meta['_type']

        item_css = 'div.my_srch div.dis_tab div.row.lst_poag'
        items = response.css(item_css)

        for item in items:
            d = {}

            d['type'] = _type
            d['name'] = item.css('h4 a ::text').extract_first()
            d['date'] = item.xpath('div[contains(@class, "date")]/text()').extract_first()
            d['date'] = dateparser.parse(d['date'], date_formats=['%d/%m/%Y']).date().strftime("%Y-%m-%d")

            yield d

        if items:
            page = get_next_page_url(response.url)
            yield response.follow(page, meta={'_type': _type}, callback=self.parse)


def get_next_page_url(url):
    p = list(urlparse(url))
    q = dict(parse_qsl(p[4]))

    if 'page' not in q:
        q['page'] = 2
    else:
        q['page'] = int(q['page']) + 1

    p[4] = urlencode(q)
    url = urlunparse(p)

    return url
