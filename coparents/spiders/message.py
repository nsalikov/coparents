# -*- coding: utf-8 -*-
import re
import json
import string
import random
import scrapy
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from ..helpers import is_login, get_recipient_id


class MessageSpider(scrapy.Spider):
    name = 'message'
    allowed_domains = ['coparents.com']
    login_url = 'https://www.coparents.com/login.php'
    compose_url = 'https://www.coparents.com/compose.php?recipient={}'
    body = """------WebKitFormBoundary{rnd}\r\nContent-Disposition: form-data; name="frm"\r\n\r\nfrmCompose\r\n------WebKitFormBoundary{rnd}\r\nContent-Disposition: form-data; name="chknotify"\r\n\r\n1\r\n------WebKitFormBoundary{rnd}\r\nContent-Disposition: form-data; name="chkinclude"\r\n\r\n0\r\n------WebKitFormBoundary{rnd}\r\nContent-Disposition: form-data; name="txtsubject"\r\n\r\n{subject}\r\n------WebKitFormBoundary{rnd}\r\nContent-Disposition: form-data; name="txtrecipient"\r\n\r\n{recipient}\r\n------WebKitFormBoundary{rnd}\r\nContent-Disposition: form-data; name="txtmessage"\r\n\r\n{message}\r\n------WebKitFormBoundary{rnd}\r\nContent-Disposition: form-data; name="txtimage"; filename=""\r\nContent-Type: application/octet-stream\r\n\r\n\r\n------WebKitFormBoundary{rnd}\r\nContent-Disposition: form-data; name="tnimage"\r\n\r\n\r\n------WebKitFormBoundary{rnd}\r\nContent-Disposition: form-data; name="btnsend"\r\n\r\nSend\r\n------WebKitFormBoundary{rnd}--\r\n"""

    custom_settings = {
        'DOWNLOAD_DELAY': 1,
        'ITEM_PIPELINES': {},
    }


    def start_requests(self):
        self.date = datetime.now().isoformat()

        self.login = self.settings.get('AUTOLOGIN_USERNAME')
        self.password = self.settings.get('AUTOLOGIN_PASSWORD')

        self.headers = self.settings.get('DEFAULT_REQUEST_HEADERS')
        self.headers['User-Agent'] = self.settings.get('USER_AGENT')

        self.headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3'
        self.headers['Origin'] = 'https://www.coparents.com'
        self.headers['Sec-Fetch-Mode'] = 'navigate'
        self.headers['Sec-Fetch-Site'] = 'same-origin'
        self.headers['Sec-Fetch-User'] = '?1'

        self.msgs = []

        self.msgs_file = self.settings.get('MSGS_FILE')
        with open(self.msgs_file) as fin:
            for line in fin:
                d = json.loads(line)
                d['url'] = 'https://www.coparents.com/' + d['name']
                self.msgs.append(d)

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'txtusername': self.login,
                    'txtpassword': self.password,
                    'rememberme': 'on',
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if is_login(response):
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        for msg in self.msgs:
            meta = {'msg': msg}
            yield scrapy.Request(msg['url'], meta=meta, callback=self.parse_profile)


    def parse_profile(self, response):
        msg = response.meta['msg']
        recipient = get_recipient_id(response)
        rnd = ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=16))

        url = self.compose_url.format(recipient)

        headers = self.headers
        headers['Content-Type'] = 'multipart/form-data; boundary=----WebKitFormBoundary{}'.format(rnd)
        headers['Referer'] = url

        body = self.body.format(rnd=rnd, recipient=recipient, subject=msg['subject'], message=re.sub('\n', r'\r\n', msg['msg']))

        yield scrapy.Request(url, method='POST', headers=headers, body=body, callback=self.parse, dont_filter=True)


    def parse(self, response):
        # inspect_response(response, self)

        return
