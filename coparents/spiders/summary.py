# -*- coding: utf-8 -*-
import re
import scrapy
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from ..helpers import _strip, add_domain, fix_image_url, is_login, handle_params, fix_sortorder


class SummarySpider(scrapy.Spider):
    name = 'summary'
    allowed_domains = ['coparents.com']
    login_url = 'https://www.coparents.com/login.php'
    search_url = 'https://www.coparents.com/advsearch.php?cursectionid=0&sectionid=0&userid=&advsearch=Search&chgcntry=&srchusername=&srchlookgender%5B%5D={gender}&{profile_type}&srchlookage={age_from}%3B{age_to}&srchlookcountry={country}&srchlookcounty=AA&srchlookstate_province=&srchlookcity=&srchlookzip=&results_per_page=50'


    def start_requests(self):
        self.date = datetime.now().isoformat()

        self.login = self.settings.get('AUTOLOGIN_USERNAME')
        self.password = self.settings.get('AUTOLOGIN_PASSWORD')

        handle_params(self)

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'txtusername': self.login,
                    'txtpassword': self.password,
                    'rememberme': 'on',
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if is_login(response):
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        return scrapy.Request(self.search_url, callback=self.set_sortorder)


    def set_sortorder(self, response):
        # inspect_response(response, self)

        pages_css = 'div.pating a ::attr(href)'
        first_page = response.css(pages_css).extract_first()

        if not first_page:
            raise CloseSpider("Search is empty")

        first_page = fix_sortorder(first_page, self.sort)

        return response.follow(first_page, callback=self.parse_search, dont_filter=True)


    def parse_search(self, response):
        # inspect_response(response, self)

        if not is_login(response):
            raise CloseSpider("Session is expired.")

        items_css = 'div.my_srch div.frsoi'
        items = response.css(items_css)

        for item in items:
            if self.last > 0:
                self.last = self.last - 1

                d = {}

                d['scraping_date'] = self.date
                d['url'] = item.css('div.ovidois h4 a ::attr(href)').extract_first()
                d['image_urls'] = item.css('div.omg img ::attr(src)').extract_first()
                d['name'] = _strip(item.css('div.ovidois h4 a ::text').extract_first())

                last_login = item.css('div.uper_woid font ::text').extract_first()
                last_login = last_login.replace('Last login: ', '').strip() if last_login else ''
                d['last_login'] = last_login

                fst, snd = item.css('div.ovidois b ::text').extract()

                sex, age = [t.strip() for t in fst.split(',')]
                location = re.sub('[\r\n\t\xa0 ]+', ' ', snd).strip()

                d['sex'] = sex
                d['age'] = age
                d['location'] = location

                d['looking'] = _strip(item.css('div.ovidois em ::text').extract_first())
                d['description'] = item.css('div.frsoi>p ::text').extract_first()

                d['image_urls'] = item.css('div.omg img ::attr(src)').extract()
                d['image_urls'] = [add_domain('https://www.coparents.com/', img) for img in d['image_urls']]

                yield d

        pages_css = 'div.pating a ::attr(href)'
        pages = response.css(pages_css).extract()

        for page in pages:
            if self.last > 0:
                yield response.follow(page, callback=self.parse_search)
