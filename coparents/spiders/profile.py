# -*- coding: utf-8 -*-
import re
import scrapy
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from ..helpers import _strip, add_domain, fix_image_url, is_login, handle_params, fix_sortorder


class ProfilesSpider(scrapy.Spider):
    name = 'profile'
    allowed_domains = ['coparents.com']
    login_url = 'https://www.coparents.com/login.php'
    search_url = 'https://www.coparents.com/advsearch.php?cursectionid=0&sectionid=0&userid=&advsearch=Search&chgcntry=&srchusername=&srchlookgender%5B%5D=F&question%5B38%5D%5B%5D=10723&question%5B38%5D%5B%5D=10724&srchlookage=19%3B40&srchlookcountry=AA&srchlookcounty=AA&srchlookstate_province=&srchlookcity=&srchlookzip=&results_per_page=50'


    def start_requests(self):
        self.date = datetime.now().isoformat()

        self.login = self.settings.get('AUTOLOGIN_USERNAME')
        self.password = self.settings.get('AUTOLOGIN_PASSWORD')

        self.profiles = set()

        self.profiles_file = self.settings.get('PROFILES_FILE')
        with open(self.profiles_file) as fin:
            for line in fin:
                line = line.strip()
                if not line:
                    continue
                if not line.startswith('http'):
                    line = 'https://www.coparents.com/' + line.lstrip('/')
                self.profiles.add(line)

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'txtusername': self.login,
                    'txtpassword': self.password,
                    'rememberme': 'on',
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if is_login(response):
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        for profile in self.profiles:
            yield scrapy.Request(profile, callback=self.parse_profile)


    def parse_profile(self, response):
        # inspect_response(response, self)

        if not is_login(response):
            raise CloseSpider("Session is expired.")

        d = {}

        d['scraping_date'] = self.date
        d['url'] = response.url

        d['name'] = _strip(' '.join(response.css('div.tiblke h1 ::text').extract()))

        if 'last_login' in response.meta:
            d['last_login'] = response.meta['last_login']
        else:
            d['last_login'] = ''

        (sex, age, location) = [re.sub('[\r\n\t\xa0 ]+', ' ', text).strip() for text in response.css('div.tiblke span ::text').extract()]
        d['sex'] = sex
        d['age'] = age
        d['location'] = location
        d['looking'] = '\n'.join(filter(None, [re.sub('[\r\n\t\xa0 ]+', ' ', text).strip() for text in response.css('div.tiblke div.imglpog ::text').extract()]))
        d['description'] = '\n'.join(filter(None, [re.sub('[\r\n\t\xa0 ]+', ' ', text).strip() for text in response.css('div.heding p.pto ::text').extract()]))

        d['details'] = {}

        lis = response.css('div.heding ul li')
        for li in lis:
            label = _strip(' '.join(filter(None, [s.strip().rstrip(':') for s in li.css('label ::text').extract()])))
            value = _strip('\n'.join(filter(None, [s.strip() for s in li.css('b ::text').extract()])))

            if label and value:
                d['details'][label] = value

        d['image_urls'] = response.css('div.main_img img ::attr(src)').extract()
        d['image_urls'].extend(response.css('div.sld_img img ::attr(longdesc)').extract())

        d['image_urls'] = [add_domain('https://www.coparents.com/', img) for img in d['image_urls']]
        d['image_urls'] = list(filter(None, [fix_image_url(img) for img in d['image_urls']]))

        yield d
