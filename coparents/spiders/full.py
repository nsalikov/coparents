# -*- coding: utf-8 -*-
import re
import scrapy
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from ..helpers import _strip, add_domain, fix_image_url, is_login, handle_params, fix_sortorder


class SearchSpider(scrapy.Spider):
    name = 'full'
    allowed_domains = ['coparents.com']
    login_url = 'https://www.coparents.com/login.php'
    search_url = 'https://www.coparents.com/advsearch.php?cursectionid=0&sectionid=0&userid=&advsearch=Search&chgcntry=&srchusername=&srchlookgender%5B%5D={gender}&{profile_type}&srchlookage={age_from}%3B{age_to}&srchlookcountry={country}&srchlookcounty=AA&srchlookstate_province=&srchlookcity=&srchlookzip=&results_per_page=50'


    def start_requests(self):
        self.date = datetime.now().isoformat()

        self.login = self.settings.get('AUTOLOGIN_USERNAME')
        self.password = self.settings.get('AUTOLOGIN_PASSWORD')

        handle_params(self)

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'txtusername': self.login,
                    'txtpassword': self.password,
                    'rememberme': 'on',
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if is_login(response):
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        return scrapy.Request(self.search_url, callback=self.set_sortorder)


    def set_sortorder(self, response):
        # inspect_response(response, self)

        pages_css = 'div.pating a ::attr(href)'
        first_page = response.css(pages_css).extract_first()

        if not first_page:
            raise CloseSpider("Search is empty")

        first_page = fix_sortorder(first_page, self.sort)

        return response.follow(first_page, callback=self.parse_search, dont_filter=True)


    def parse_search(self, response):
        # inspect_response(response, self)

        if not is_login(response):
            raise CloseSpider("Session is expired.")

        items_css = 'div.my_srch div.frsoi'
        items = response.css(items_css)

        for item in items:
            if self.last > 0:
                self.last = self.last - 1

                url = item.css('div.ovidois h4 a ::attr(href)').extract_first()
                last_login = item.css('div.uper_woid font ::text').extract_first()
                last_login = last_login.replace('Last login: ', '').strip() if last_login else ''
                meta = {'last_login': last_login}

                yield response.follow(url, meta=meta, callback=self.parse_profile)

        pages_css = 'div.pating a ::attr(href)'
        pages = response.css(pages_css).extract()

        for page in pages:
            if self.last > 0:
                yield response.follow(page, callback=self.parse_search)


    def parse_profile(self, response):
        # inspect_response(response, self)

        if not is_login(response):
            raise CloseSpider("Session is expired.")

        d = {}

        d['scraping_date'] = self.date
        d['url'] = response.url

        d['name'] = _strip(' '.join(response.css('div.tiblke h1 ::text').extract()))
        d['last_login'] = response.meta['last_login']

        (sex, age, location) = [re.sub('[\r\n\t\xa0 ]+', ' ', text).strip() for text in response.css('div.tiblke span ::text').extract()]
        d['sex'] = sex
        d['age'] = age
        d['location'] = location
        d['looking'] = '\n'.join(filter(None, [re.sub('[\r\n\t\xa0 ]+', ' ', text).strip() for text in response.css('div.tiblke div.imglpog ::text').extract()]))
        d['description'] = '\n'.join(filter(None, [re.sub('[\r\n\t\xa0 ]+', ' ', text).strip() for text in response.css('div.heding p.pto ::text').extract()]))

        d['details'] = {}

        lis = response.css('div.heding ul li')
        for li in lis:
            label = _strip(' '.join(filter(None, [s.strip().rstrip(':') for s in li.css('label ::text').extract()])))
            value = _strip('\n'.join(filter(None, [s.strip() for s in li.css('b ::text').extract()])))

            if label and value:
                d['details'][label] = value

        d['image_urls'] = response.css('div.main_img img ::attr(src)').extract()
        d['image_urls'].extend(response.css('div.sld_img img ::attr(longdesc)').extract())

        d['image_urls'] = [add_domain('https://www.coparents.com/', img) for img in d['image_urls']]
        d['image_urls'] = list(filter(None, [fix_image_url(img) for img in d['image_urls']]))

        yield d
