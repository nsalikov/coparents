# -*- coding: utf-8 -*-
import re
import json
import string
import random
import scrapy
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.shell import inspect_response
from ..helpers import is_login, get_recipient_id


class WinkSpider(scrapy.Spider):
    name = 'wink'
    allowed_domains = ['coparents.com']
    login_url = 'https://www.coparents.com/login.php'
    wink_url = 'https://www.coparents.com/sendwinks.php?ref_id={}&rtnurl=showprofile.php'

    custom_settings = {
        'DOWNLOAD_DELAY': 1,
        'ITEM_PIPELINES': {},
    }


    def start_requests(self):
        self.date = datetime.now().isoformat()

        self.login = self.settings.get('AUTOLOGIN_USERNAME')
        self.password = self.settings.get('AUTOLOGIN_PASSWORD')

        self.headers = self.settings.get('DEFAULT_REQUEST_HEADERS')
        self.headers['User-Agent'] = self.settings.get('USER_AGENT')

        self.headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3'
        self.headers['Origin'] = 'https://www.coparents.com'
        self.headers['Sec-Fetch-Mode'] = 'navigate'
        self.headers['Sec-Fetch-Site'] = 'same-origin'
        self.headers['Sec-Fetch-User'] = '?1'

        self.wink = set()

        self.wink_file = self.settings.get('WINK_FILE')
        with open(self.wink_file) as fin:
            for line in fin:
                line = line.strip()
                if not line:
                    continue
                if not line.startswith('http'):
                    line = 'https://www.coparents.com/' + line.lstrip('/')
                self.wink.add(line)

        yield scrapy.Request(self.login_url, callback=self.do_login, dont_filter=True)


    def do_login(self, response):

        formdata = {
                    'txtusername': self.login,
                    'txtpassword': self.password,
                    'rememberme': 'on',
                }

        r = scrapy.FormRequest.from_response(response, formdata=formdata, callback=self.check_login)

        return r


    def check_login(self, response):
        # inspect_response(response, self)

        if is_login(response):
            return self.after_login(response)
        else:
            return self.login_fail(response)


    def login_fail(self, response):
        # self.logger.critical("Unable to login")
        raise CloseSpider("Something wrong. Unable to login.")


    def after_login(self, response):
        for wink in self.wink:
            yield scrapy.Request(wink, callback=self.parse_profile)


    def parse_profile(self, response):
        recipient = get_recipient_id(response)
        url = self.wink_url.format(recipient)

        yield scrapy.Request(url, callback=self.parse)


    def parse(self, response):
        # inspect_response(response, self)

        return
